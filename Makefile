build:
	@docker run --rm -it --volume="$(shell pwd):/srv/jekyll" --volume="$(shell pwd)/vendor/bundle:/usr/local/bundle" --env JEKYLL_ENV=production jekyll/jekyll:3.8 jekyll build
serve:
	@docker run --rm --volume="$(shell pwd):/srv/jekyll" --volume="$(shell pwd)/vendor/bundle:/usr/local/bundle" --env JEKYLL_ENV=development -p 4000:4000 jekyll/jekyll:3.8 jekyll serve
bundle:
	@tar -C _site -czvf pge-website-bundle.tar.gz .